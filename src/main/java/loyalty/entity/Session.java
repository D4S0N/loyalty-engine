package loyalty.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Session {
    private long initialPaymentSum;
    private long resultPaymentSum;
    private String gift;
    private String promoCode;
}
