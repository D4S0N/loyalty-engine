package loyalty;

import loyalty.domain.*;
import loyalty.operator.ConditionalOperator;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

    public static void main(String[] args) {
        List<Campaign> campaigns = createCampaigns();
        campaigns.forEach(Campaign::activate);
    }

    private static List<Campaign> createCampaigns() {
        List<Rule> rules = createRules();
        Campaign campaign = Campaign.builder()
                .name("Campaign 40% discount")
                .rules(rules)
                .build();
        return Stream.of(campaign)
                .collect(Collectors.toList());
    }

    private static List<Rule> createRules() {
        List<Condition> conditions = createConditions();
        List<ConditionalOperator> operators = createOperators();
        List<Effect> effects = createEffects();

        Rule rule1 = Rule.builder()
                .conditions(conditions)
                .operators(operators)
                .effects(effects)
                .build();

        Rule rule2 = Rule.builder()
                .conditions(conditions)
                .operators(operators)
                .effects(effects)
                .build();

        return Stream.of(rule1, rule2)
                .collect(Collectors.toList());
    }

    private static List<Condition> createConditions() {
        Condition condition1 = Condition.builder()
                .value("AGE")
                .operator("GREATER THAN")
                .bound("18")
                .build();
        Condition condition2 = Condition.builder()
                .value("AGE")
                .operator("GREATER THAN")
                .bound("18")
                .build();
        Condition condition3 = Condition.builder()
                .value("AGE")
                .operator("GREATER THAN")
                .bound("18")
                .build();
        Condition condition4 = Condition.builder()
                .value("AGE")
                .operator("GREATER THAN")
                .bound("18")
                .build();
        Condition condition5 = Condition.builder()
                .value("AGE")
                .operator("GREATER THAN")
                .bound("18")
                .build();
        Condition condition6 = Condition.builder()
                .value("AGE")
                .operator("GREATER THAN")
                .bound("18")
                .build();

        return Stream.of(condition1, condition2, condition3, condition4, condition5, condition6)
                .collect(Collectors.toList());
    }

    private static List<ConditionalOperator> createOperators() {
        ConditionalOperator operator1 = ConditionalOperator.OR;
        ConditionalOperator operator2 = ConditionalOperator.AND;
        ConditionalOperator operator3 = ConditionalOperator.AND;
        ConditionalOperator operator4 = ConditionalOperator.OR;
        ConditionalOperator operator5 = ConditionalOperator.AND;

        return Stream.of(operator1, operator2, operator3, operator4, operator5)
                .collect(Collectors.toList());
    }

    private static List<Effect> createEffects() {
        Effect effect1 = Effect.builder()
                .build();
        Effect effect2 = Effect.builder()
                .build();
        Effect effect3 = Effect.builder()
                .build();

        return Stream.of(effect1, effect2, effect3)
                .collect(Collectors.toList());
    }


}
