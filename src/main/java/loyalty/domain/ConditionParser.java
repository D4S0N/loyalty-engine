package loyalty.domain;

import loyalty.operator.Operator;
import loyalty.type.ValueType;

public class ConditionParser {

    private Context context;

    public Condition parse(String sourceValue, String sourceOperator, String sourceBound) {
        ValueType value = parseValue(sourceValue);
        Operator operator = parseOperator(sourceOperator);
        ValueType bound = parseBound(sourceBound);
        return Condition.builder()
                .value(value)
                .operator(operator)
                .bound(bound)
                .build();
    }

    private ValueType parseValue(String sourceValue) {
        return null;
    }

    private Operator parseOperator(String sourceOperator) {
        return null;
    }

    private ValueType parseBound(String sourceBound) {
        return null;
    }
}
