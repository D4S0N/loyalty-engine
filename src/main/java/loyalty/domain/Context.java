package loyalty.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import loyalty.entity.Session;
import loyalty.entity.User;
import loyalty.operator.IsGreaterThanOperator;
import loyalty.operator.IsLessThanOperator;
import loyalty.operator.Operator;
import loyalty.operator.StartsWithOperator;
import loyalty.type.DateType;
import loyalty.type.IntType;
import loyalty.type.StringType;
import loyalty.type.ValueType;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Context {

    private static TreeMap<String, Object> tree;
    private static Map<ValueType, List<Object>> operatorsMap;

    public static TreeMap create() {
        tree.put("User.age", DateType.class);
        tree.put("User.balance", DateType.class);
        tree.put("User.loyaltyPoints", DateType.class);
        tree.put("User.tariff", DateType.class);
        tree.put("User.socialGroup", StringType.class);
        tree.put("User.referenceRate", IntType.class);
        tree.put("Session.initialPaymentSum", IntType.class);
        tree.put("Session.resultPaymentSum", IntType.class);
        tree.put("Session.gift", StringType.class);
        tree.put("Session.promoCode", StringType.class);
        return tree;
    }

    public static Map<ValueType, List<Object>> createOperatorsMapping() {
        operatorsMap.get(StringType.class).add(StartsWithOperator.class);
        operatorsMap.get(IntType.class).add(IsGreaterThanOperator.class);
        operatorsMap.get(IntType.class).add(IsLessThanOperator.class);
        return operatorsMap;
    }

    public static Object find(String value) {
        return tree.get(value);
    }

    public static List<Object> getApplicableOperators(ValueType type) {
        return operatorsMap.get(type);
    }
}
