package loyalty.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import loyalty.state.CampaignState;
import loyalty.state.RuleState;

import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Campaign {

    private long id;
    private String name;
    private long budget;
    private long usages;
    private CampaignState state;
    private Date start;
    private Date end;
    private List<Rule> rules;

    public void activate() {
        boolean areAllRulesAccepted = rules.stream()
                .allMatch(rule -> rule.process().equals(RuleState.ACCEPTED));
        if (areAllRulesAccepted) {
            state = CampaignState.ACTIVE;
        }
    }

    public void deactivate() {
        state = CampaignState.DISABLED;
    }
}
