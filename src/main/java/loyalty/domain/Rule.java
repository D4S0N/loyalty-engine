package loyalty.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import loyalty.operator.ConditionalOperator;
import loyalty.state.RuleState;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Rule {

    private RuleState state;
    private List<Condition> conditions;
    private List<ConditionalOperator> operators;
    private List<Effect> effects;

    public RuleState process() {
        combineConditions(conditions, operators);
        if (state.equals(RuleState.ACCEPTED)) {
            effects.forEach(Effect::apply);
        }
        return state;
    }

    private void combineConditions(List<Condition> conditions, List<ConditionalOperator> operators) {
        processAndOperators(conditions, operators);
        boolean isRuleAccepted = processOrOperators(conditions, operators);

        if (isRuleAccepted) {
            state = RuleState.ACCEPTED;
        } else {
            state = RuleState.REJECTED;
        }
    }

    private void processAndOperators(List<Condition> conditions, List<ConditionalOperator> operators) {
        int i = 0;
        while (i < operators.size()) {
            if (operators.get(i).equals(ConditionalOperator.AND)) {
                unionTwoConditions(conditions, operators, i);
                i--;
            }
            i++;
        }
    }

    private void unionTwoConditions(List<Condition> conditions, List<ConditionalOperator> operators, int i) {
        conditions.set(i, Condition.builder()
                .result(conditions.get(i).calculate() && conditions.get(i + 1).calculate())
                .build());
        conditions.remove(i + 1);
        operators.remove(i);
    }

    private boolean processOrOperators(List<Condition> conditions, List<ConditionalOperator> operators) {
        return conditions.stream().anyMatch(Condition::calculate);
    }

}
