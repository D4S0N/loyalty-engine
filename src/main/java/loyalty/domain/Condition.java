package loyalty.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import loyalty.operator.Operator;
import loyalty.type.ValueType;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Condition {

    private ValueType value;
    private Operator operator;
    private ValueType bound;
    private boolean result = false;


    public Condition(ValueType value, Operator operator, ValueType bound) {
        this.value = value;
        this.operator = operator;
        this.bound = bound;
    }

    public boolean calculate() {
        result = operator.apply(value, bound);
        return result;
    }
}
