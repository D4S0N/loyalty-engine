package loyalty.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import loyalty.state.EffectState;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Effect {

    private EffectState effectState;

    public EffectState apply() {
        return effectState;
    }
}
