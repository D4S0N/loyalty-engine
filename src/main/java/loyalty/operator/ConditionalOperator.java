package loyalty.operator;

public enum  ConditionalOperator {
    AND,
    OR;

    public boolean apply(boolean first, boolean second) {
        switch (this) {
            case AND: return first && second;
            case OR: return first || second;
            default: return false;
        }
    }
}
