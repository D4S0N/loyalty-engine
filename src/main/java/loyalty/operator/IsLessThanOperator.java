package loyalty.operator;

import loyalty.type.IntType;
import loyalty.type.ValueType;

public class IsLessThanOperator implements Operator{

    @Override
    public boolean apply(ValueType value, ValueType bound) {
        return (IntType) value < (IntType) bound;
    }
}
