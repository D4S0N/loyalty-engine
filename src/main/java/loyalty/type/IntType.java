package loyalty.type;

public class IntType extends Integer implements ValueType{

    public IntType(int value) {
        super(value);
    }
}
