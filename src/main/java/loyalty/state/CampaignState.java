package loyalty.state;

public enum CampaignState {
    DISABLED,
    ACTIVE
}
