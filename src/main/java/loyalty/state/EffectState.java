package loyalty.state;

public enum EffectState {
    APPLIED,
    NOT_APPLIED
}
