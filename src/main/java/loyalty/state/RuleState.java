package loyalty.state;

public enum  RuleState {
    ACCEPTED,
    REJECTED
}
